import argparse
import numpy as np
import math
import pandas as pd

if __name__ == "__main__":
    myParser = argparse.ArgumentParser()
    myParser.add_argument("-a", help="load acquisition data", required=True)
    myParser.add_argument("-p", help="load performance data", required=True)
    args = myParser.parse_args()

    # read files
    with open(args.a, 'r') as a_file:
        Acquisition = pd.read_csv(a_file, sep="|", header=None, usecols=[0, 1, 2, 4, 8, 9, 12, 18, 22], parse_dates=True)
        Acquisition.columns = ['LOAN_ID', 'CHN', 'SELLER', 'ORIG_UPB', 'OLTV', 'OCLTV', 'B_SCORE', 'STATE', 'C_SCORE']

    with open(args.p, 'r') as p_file:
        Performance = pd.read_csv(p_file, sep="|", header=None, usecols=[0, 1, 4, 10], parse_dates=True)
        Performance.columns = ['LOAN_ID', 'MONTHLY.DATE', 'UPB', 'DELQ.STATUS']

    # Question 1
    # Choose only the most recent UPB
    Current_Performance = Performance[['LOAN_ID', 'UPB', 'DELQ.STATUS']].groupby('LOAN_ID').last().reset_index()
    # Merge Acquisition and Performance data by LOAN_ID
    UPB_perchannel = pd.concat([Acquisition[['LOAN_ID', 'CHN']], Current_Performance[['UPB']]], axis=1, join='inner')
    # Count the sum of UPBs for each channel
    R_sum = UPB_perchannel['UPB'][UPB_perchannel['CHN'] == 'R'].sum()
    B_sum = UPB_perchannel['UPB'][UPB_perchannel['CHN'] == 'B'].sum()
    C_sum = UPB_perchannel['UPB'][UPB_perchannel['CHN'] == 'C'].sum()
    print "---------------------------------------------"
    print "--                 Q(1)                    --"
    print "---------------------------------------------"
    print "Total Current UPB per Channel:"
    print "Retail: ${}".format(R_sum)
    print "Correspondent: ${}".format(B_sum)
    print "Broker: ${}".format(C_sum)
    print " "

    # Question 2
    # Merge data
    loans = pd.concat([Acquisition[['LOAN_ID', 'CHN', 'STATE']], Current_Performance[['DELQ.STATUS']]], axis=1,join='inner')
    # drop unkonwn loans
    loans = loans.drop(loans[loans['DELQ.STATUS'] == 'X'].index).reindex()
    # convert status type to integer
    loans['DELQ.STATUS'] = loans['DELQ.STATUS'].astype(int)
    # count the number of loans by status for different groups of states
    total_loans_perstate = loans['DELQ.STATUS'].groupby(loans['STATE']).count()
    current_loans_perstate = loans['DELQ.STATUS'][loans['DELQ.STATUS'] == 0].groupby(loans['STATE']).count()
    delinquencies_perstate = loans['DELQ.STATUS'][loans['DELQ.STATUS'] != 0].groupby(loans['STATE']).count()
    print "---------------------------------------------"
    print "--                 Q(2)                    --"
    print "---------------------------------------------"
    print "Total Loans per State:"
    print total_loans_perstate
    print "Current Loans per State:"
    print current_loans_perstate
    print "Delinquencies per State:"
    print delinquencies_perstate
    print " "

    # Question 3
    # count the number of loans by status for different channels
    total_loans_perchannel = loans['DELQ.STATUS'].groupby(loans['CHN']).count()
    current_loans_perchannel = loans['DELQ.STATUS'][loans['DELQ.STATUS'] == 0].groupby(loans['CHN']).count()
    delinquencies_perchannel = loans['DELQ.STATUS'][loans['DELQ.STATUS'] != 0].groupby(loans['CHN']).count()
    print "---------------------------------------------"
    print "--                 Q(3)                    --"
    print "---------------------------------------------"
    print "Total Loans per Channel:"
    print total_loans_perchannel
    print "Current Loans per Channel:"
    print current_loans_perchannel
    print "Delinquencies per Channel"
    print delinquencies_perchannel
    print " "

    # Question 4
    # drop "OTHER" type for sellers
    sellers = Acquisition.drop(Acquisition[Acquisition['SELLER'] == 'OTHER'].index)
    # count the number of loans for each seller
    sellers_count = sellers['LOAN_ID'].groupby(sellers['SELLER']).count()
    print "---------------------------------------------"
    print "--                 Q(4)                    --"
    print "---------------------------------------------"
    print "The Bigger Seller to Fannie Mae by Number of Loans:"
    print sellers_count.sort_values(ascending=False).head(1)
    print " "

    # Question 5
    # count the sum of original UPBs for each seller
    sellers_by_OrigUPB = sellers['ORIG_UPB'].groupby(sellers['SELLER']).sum()
    print "---------------------------------------------"
    print "--                 Q(5)                    --"
    print "---------------------------------------------"
    print "The Bigger Seller to Fannie Mae by Original UPB:"
    print sellers_by_OrigUPB.sort_values(ascending=False).head(1)
    print " "

    # Question 6
    # Create sub DataFrame for each FICO range, and drop unknown loans
    FICOs = pd.concat([Acquisition[['LOAN_ID', 'B_SCORE']], Current_Performance[['DELQ.STATUS']]], axis=1, join='inner')
    FICOs = FICOs.drop(FICOs[FICOs['DELQ.STATUS'] == 'X'].index).reindex()
    # convert status type to integer, fill NaN to 0
    FICOs['DELQ.STATUS'] = FICOs['DELQ.STATUS'].astype(int)
    FICOs['B_SCORE'].fillna(0, inplace=True)

    # default rate is calculated as: number of default loans over total loans
    FICO1 = FICOs[FICOs['B_SCORE'] >= 780]
    dr1 = float(FICO1['DELQ.STATUS'][FICO1['DELQ.STATUS'] >= 4].count()) / float(FICO1['DELQ.STATUS'].count())

    FICO2 = FICOs[FICOs['B_SCORE'] >= 740]
    FICO2 = FICO2[FICO2['B_SCORE'] < 780]
    dr2 = float(FICO2['DELQ.STATUS'][FICO2['DELQ.STATUS'] >= 4].count()) / float(FICO2['DELQ.STATUS'].count())

    FICO3 = FICOs[FICOs['B_SCORE'] >= 700]
    FICO3 = FICO3[FICO3['B_SCORE'] < 740]
    dr3 = float(FICO3['DELQ.STATUS'][FICO3['DELQ.STATUS'] >= 4].count()) / float(FICO3['DELQ.STATUS'].count())

    FICO4 = FICOs[FICOs['B_SCORE'] >= 660]
    FICO4 = FICO4[FICO4['B_SCORE'] < 700]
    dr4 = float(FICO4['DELQ.STATUS'][FICO4['DELQ.STATUS'] >= 4].count()) / float(FICO4['DELQ.STATUS'].count())

    FICO5 = FICOs[FICOs['B_SCORE'] >= 620]
    FICO5 = FICO5[FICO5['B_SCORE'] < 660]
    dr5 = float(FICO5['DELQ.STATUS'][FICO5['DELQ.STATUS'] >= 4].count()) / float(FICO5['DELQ.STATUS'].count())

    FICO6 = FICOs[FICOs['B_SCORE'] > 0]
    FICO6 = FICO6[FICO6['B_SCORE'] < 620]
    dr6 = float(FICO6['DELQ.STATUS'][FICO6['DELQ.STATUS'] >= 4].count()) / float(FICO6['DELQ.STATUS'].count())

    FICO7 = FICOs[FICOs['B_SCORE'] == 0]
    dr7 = float(FICO7['DELQ.STATUS'][FICO7['DELQ.STATUS'] >= 4].count()) / float(FICO7['DELQ.STATUS'].count())
    print "---------------------------------------------"
    print "--                 Q(6)                    --"
    print "---------------------------------------------"
    print "       FICO >=780 default rate: {}".format(str(dr1 * 100) + '%')
    print "740 <= FICO < 780 default rate: {}".format(str(dr2 * 100) + '%')
    print "700 <= FICO < 740 default rate: {}".format(str(dr3 * 100) + '%')
    print "660 <= FICO < 700 default rate: {}".format(str(dr4 * 100) + '%')
    print "620 <= FICO < 660 default rate: {}".format(str(dr5 * 100) + '%')
    print "  0  < FICO < 620 default rate: {}".format(str(dr6 * 100) + '%')
    print "          Missing default rate: {}".format(str(dr7 * 100) + '%')
    print " "
    # default rates per channel
    defaults_perchannel = loans['DELQ.STATUS'][loans['DELQ.STATUS'] >= 4].groupby(loans['CHN']).count()
    default_rates = defaults_perchannel / total_loans_perchannel
    print "---------------------------------------------"
    print "Default Rates per Channel:"
    print default_rates
    print " "
